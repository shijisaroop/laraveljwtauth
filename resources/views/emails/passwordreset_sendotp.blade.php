<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear {{ $user->firstname }} {{ $user->lastname }},
    <br/><br/>
    Your One Time Password (OTP) for reseting password  is <b>{{ $userOtp->token }} </b>. 
    <br/><br/>
    This OTP is valid for 15 minutes or 1 successful attempt whichever is earlier. 
    <br/><br/>
    Please do not share this One Time Password with anyone.

    <br/><br/>
    Best Regards<br/>
    CompanyName
</div>

</body>
</html>