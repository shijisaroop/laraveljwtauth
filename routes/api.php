<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

/* Route::group([
    'namespace' => 'Api',
    'prefix' => 'api'
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('resetpassword/sendotp', 'PasswordController@sendOtpEmail');
    Route::post('password/reset', 'PasswordController@reset');

});
Route::group([
    'middleware' => 'jwt.verify'
], function ($router) {
    Route::post('api/logout', 'AuthController@logout');
    Route::get('api/refreshtoken', 'AuthController@refreshtoken');
    Route::get('whoami', 'AuthController@whoami');
});
 */

$router->post('login', ['uses' => 'Api\AuthController@login']);
$router->post('register', ['uses' => 'Api\AuthController@register']);

$router->post('resetpassword/sendotp', 'Api\PasswordController@sendOtpEmail');
$router->post('password/reset', 'Api\PasswordController@reset');

$router->group(['middleware' => ['jwt.verify']], function () use ($router) {    
    $router->get('logout', ['uses' => 'Api\AuthController@logout']);
    $router->get('refreshtoken', ['uses' => 'Api\AuthController@refreshtoken']);
});