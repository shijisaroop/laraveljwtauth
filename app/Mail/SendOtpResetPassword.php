<?php

namespace App\Mail;

use App\User;
use App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOtpResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $userOtp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, PasswordReset $userOtp)
    {
        $this->user = $user;
        $this->userOtp = $userOtp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Reset Password OTP")
                    ->view('emails.passwordreset_sendotp');
    }
}